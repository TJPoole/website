---
title: "Coronavirus"
date: 2020-03-18T16:39:56Z
draft: false
---

> Hi all, we've created a Whatsapp group as a church/corps group for all we know who has WhatsApp. Apologies if we've missed anyone out - if you notice anyone, please message us privately.
>
>If you haven't already heard, we have been told by the central Salvation Army that all activities including weekday programme & Sunday worship need to be cancelled until further notice due to coronavirus. We are deeply saddened by this but understand it is to keep people safe. This also gives us an opportunity to be creative about how we gather together & we are considering options online for instance for Sunday worship. We'll let you know how we'll go forward. If you have any suggestions please say.
