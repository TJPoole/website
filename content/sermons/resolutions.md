---
title: "Broken Windows, Broken Lives"
date: 2019-12-29T09:33:34Z
draft: false
---

## Question
*Do you have any resolutions for the New Year?*

## Passage
[Romans 12:9-21](https://www.biblegateway.com/passage/?search=Romans+12:9-21&version=NIV)

## Audio
[Listen to the sermon recording](/audio/2019-12-29 11-17 am Sermon.m4a).

## Slides
<iframe src="/slides/2019-12-Broken windows - broken lives.pdf" style="border:0px #ffffff none;" scrolling="no" frameborder="1" marginheight="0px" marginwidth="0px" height="400px" width="600px" allowfullscreen></iframe>

## Notes

I don’t know what sort of week you’ve had. I would say that mine has been ‘mixed’!

I’ve had moments of great joy: visiting my Dad in Formby, having a Christmas meal with friends at our house on Christmas day.

On the other hand, I’ve had endless sickness to cope with amongst my team at work 
I lost my credit card due to an error at the garage when I was refuelling my car and had to go through the kerfuffle of getting that replaced
My computer at home has suddenly stopped working – I use it for both my NHS work and also writing books so that’s a pain
My car has developed a fault which meant I’ve had to rent a hire car so I can still get to work because the dealership can’t even look at it until Monday 6th January because of the Christmas and New Year holidays

However, these are all very much first world problems – nobody has died, I still have a warm and comfortable home to live in and enough food to eat.

And so it was, John was giving me a lift to pick up the hire car on Friday lunchtime, when we drove past the Sally Army church centre and saw this…

John dropped me off to sort out the hire car and getting to work and came back to have a look at the door and the office window and decide what needed to be done.

I don’t know what you think when you see something like that.

My first thought was something like: what a pain, that’s going to take ages to sort out.

And then inevitably you start thinking: 
I wonder how long it’s been like that (it must have happened sometime between the Carol Service on the 24th and Friday lunchtime 

And then you start thinking:

I wonder who did it and why?

Whoever did it, really really intended to do it, because the door and window had been hit with quite some force.

Was it kids, just ‘having a laugh’ and causing a load of hassle in the process?

Was it someone who was disappointed and let down because the church wasn’t open on Christmas Day?

Was it someone who just hated churches in general, or the Salvation Army in particular?

Was it someone who was just full of frustration and who just had to take it out on someone or something?

How do you feel about someone or some people who would do that?

And then I thought of Christmas Day two years ago in 2017:

We’d been planning to have a Christmas Party in the church centre for people who hadn’t got anywhere else to go on Christmas Day

It wasn’t going to be a full turkey dinner with all the trimmings, but it was going to be an opportunity to make people welcome, have something to eat, play some games and generally celebrate.

We were shocked to find we weren’t allowed up Prescott Road, there were barriers put up by the police

Following a police chase, a young man had driven his car into the bus stop and then into the front of the Sally Army church building; he was injured, his relative in the front passenger seat had lost his life

We were allowed to get some things out of the church centre by the police and the party decamped to our house. But we were very mindful of the tragedy that had unfolded in front of us, and that someone that day was experiencing huge sadness.

The case eventually went to Liverpool Crown Court. The court heard:

The sister of a man killed in a car crash while being chased by police said losing him is “one of the hardest things I have ever had to cope with.”

Thomas Clayton, 34, was fatally injured in a collision in Old Swan on Christmas Day, The car was being driven by his younger cousin Carl, who, reached speeds of 99mph in 30mph zones.
The two-mile police pursuit ended after the high-powered BMW Series 1 car, which had jumped red lights, and careered onto the wrong side of the carriageway, ploughed into a bus stop and the Salvation Army building.

In a statement read to Liverpool Crown Court Thomas Clayton’s sister said: "I’ve been left devastated by what happened and none of us will truly come to terms with the loss of our brother. "Carl took Tom back to Liverpool because Tom was intent on taking a present to little Tom [his son] for Christmas Day

In court on November 16th 2018 Carl Clayton, 24, was handed a seven year prison sentence for his “appalling and lunatic-like” driving in the early hours of Christmas Day. He had previously been disqualified from driving on multiple occasions, and had once been pursued by police as he rode away from officers on a moped through Wavertree.

Carl, who was flung from the driver’s seat to the rear of the vehicle as the BMW flipped onto its roof, says he has been “living a life of misery which was entirely his own fault”. Liverpool Crown Court heard that Carl has made four suicide attempts but has three times refused medication being offered to him.

So back to our smashed window

I wonder who did it and why?

Was it kids, just ‘having a laugh’, was it someone who was disappointed and let down by the church

Was it someone who just hated churches in general, or the Salvation Army in particular?

Was it someone who was just full of frustration and who just had to take it out on someone or something?

Or was it someone who was full of grief about the events 2 years ago who just lashed out in hurt and frustration?

We don’t know. It might not have been any of these things.

But how you feel about it will probably be affected by knowing the back story of the person who did it. And that is the key.

Be sincere in love
WE mustn’t look at what you can gain by showing favour to people – we must try to love people because of who they are, because we can see the God-person inside them, the God-given potential in them

Hate the consequences of evil
The reading says ‘hate what is evil’ – it really means that we should hate the consequences of evil. Paul uses a really strong word ‘hate’ because we shouln’t be luke-warm where evil is concerned but should have powerful feelings about trying to loosen its grip on people

Be joyful in hope
I think it was William Barclay who said ‘If Christianity isn’t attractive, it is nothing’. Some branches of Christianity behave as if they’ve had all the life sucked out of them – all they can see is the negative in people. John yesterday about trainers in church. 
We have LIFE to offer people

Practise hospitality
Welcome people whoever they are, and whatever they’ve done. Different sides of the same coin – Paul continues:
Bless those who persecute you – if your enemy is hungry, feed him; if your enemy is thirsty give him something to drink
Rejoice with those who rejoice
Mourn with those who mourn – often helping people is not about offering solutions, is often about listening to their story

Do not repay anyone evil for evil
Don’t take revenge – we can’t possibly do that – only God knows a person’s back story – what’s driven them to do the things they’ve done

That video was on the BBC News website on December 20th

Surprising because the BBC hasn’t been particularly pro-Christianity recently

The guy in the video says

I felt like a parasite in society

I wasn’t worthy of any help

He did lots of courses – perhaps opportunities he hadn’t had before

But he clearly says that the thing that really turned his life around:

I went to church and they told me that they loved me

It was really emotional but it was really sort of inspiring and transformative for me

Being able to hold your head up in society, believing that you are worth something is life-changing

This is Steve Chalke – who is a somewhat controversial figure in some church circles

But even those who think his Bible interpretation is incorrect can’t deny that the Oasis Trust which Steve founded, does a huge amount of good in society


The Oasis Trust now runs hundreds of schools across the world

Steve says that often when kids are going off the rails, people get frustrated and shout ‘What’s WRONG with you!!!’

A better question to ask is ‘What HAPPENED to you’. What has made you behave the way you do? There’s usually something broken there.

And the testimony of millions of people says that what wins through in the end is love and care, not threats and abuse

How does God teach us stuff? By exposing us to experiences that make us learn to behave differently.

God teaches us to love by putting some unlovely people around us…

I really don’t like the use of the word ‘unlovely’ on the above slide – every single person is made in God’s image, but that may not exactly be what’s ‘up front’

My new year’s resolution is to try to learn to love even the most challenging people more

To try to see the God-person, the human being made in God’s image, in even the people I find most difficult to love

Because that’s what Jesus did, and that’s what changes the world.

