---
title: "DemoJ"
date: 2021-01-15T15:37:53Z
draft: true
text: ""
preacher: ""
---

## Question
...

## Video
...

## Passage
[Ref](https://www.biblegateway.com/passage/?search=Isaiah+11%3A1-10&version=NIV)

> ...text...

## Audio
[Listen to the sermon recording](/audio/...m4a).

## Slides
<iframe src="/slides/....pdf" style="border:0px #ffffff none;" scrolling="no" frameborder="1" marginheight="0px" marginwidth="0px" height="500px" width="600px" allowfullscreen></iframe>
