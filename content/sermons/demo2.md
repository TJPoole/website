---
title: "Epiphany: The Cosmic Treasure Hunt"
date: 2021-01-3T21:27:25Z
draft: true
text: "Matthew 8:11, Isaiah 47:13"
preacher: "Jenni Tomlin"
---

## Question
...

## Video
...https://youtu.be/nKJtIcQDBno

## Passage
[Ref](https://www.biblegateway.com/passage/?search=Isaiah+11%3A1-10&version=NIV)

> ...text...

## Audio
[Listen to the sermon recording](/audio/...m4a).

## Slides
<iframe src="/slides/....pdf" style="border:0px #ffffff none;" scrolling="no" frameborder="1" marginheight="0px" marginwidth="0px" height="500px" width="600px" allowfullscreen></iframe>
