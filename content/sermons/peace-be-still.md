---
title: "Peace, be still!"
date: 2020-03-01T16:27:16Z
draft: false
text: "Mark 4:35-41"
preacher: "Jenni Tomlin"
---

This was our monthly **Story Sunday**.

## Passage
[Mark 4:35-41](https://www.biblegateway.com/passage/?search=Mark+4%3A35-41&version=NIV)

> That day when evening came, he said to his disciples, “Let us go over to the other side.” Leaving the crowd behind, they took him along, just as he was, in the boat. There were also other boats with him. A furious squall came up, and the waves broke over the boat, so that it was nearly swamped. Jesus was in the stern, sleeping on a cushion. The disciples woke him and said to him, “Teacher, don’t you care if we drown?”
>
> He got up, rebuked the wind and said to the waves, “Quiet! Be still!” Then the wind died down and it was completely calm.
>
> He said to his disciples, “Why are you so afraid? Do you still have no faith?”
> 
> They were terrified and asked each other, “Who is this? Even the wind and the waves obey him!”

## Audio
[Listen to this new song: Peace, be still!](/audio/2020-03-01 11-34 am-Peace-Be-Still-song.m4a).

In the middle of the night,
Jesus is my light;
In the middle of the mess,
Jesus is my rest;
In the middle of the storm,
Jesus is strong--
So Goodbye Fear!
Jesus is here!

Peace, be still!
When I am afraid
Peace, be still!
I call on Jesus' name
Peace, be still!
I am safe and loved
Yes I am

Peace, be still!
No more anxiety
Peace, be still!
Jesus cares for me
Peace, be still!
I am safe and loved
Yes I am
